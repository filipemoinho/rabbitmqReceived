package com.itau.received;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class GravaArquivo {
	
	public static void SaveLog(String message) throws IOException {
		BufferedWriter writer = new BufferedWriter(new FileWriter("log.txt", true));
		writer.write(message);

		writer.close();
	}

}
